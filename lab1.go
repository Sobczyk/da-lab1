package main

import "fmt"

var channels []chan Message
var neighbours [][]int
var done chan bool
var done2 chan bool

const search = -1
const youAreMyFather = -2

type Message struct {
	msgType int
	sender  int
}

func root() {
	var maxNumberOfChildren = len(neighbours[0])
	var currentNumberOfChildren = 0
	var children = make([]int, maxNumberOfChildren)
	for i := 0; i < maxNumberOfChildren; i++ {
		children[i] = -1
	}

	// send `search` to all neighbours
	for i := 0; i < len(neighbours[0]); i++ {
		var neighbour = neighbours[0][i]
		channels[neighbour] <- Message{search, 0}
	}

	done <- true

	for {
		msg, more := <-channels[0]
		if more {
			// received 'you are my father'
			if msg.msgType == youAreMyFather {
				children[currentNumberOfChildren] = msg.sender
				currentNumberOfChildren++
			}
		} else {
			break
		}
	}
	fmt.Println("0 : parent = 0, children = ", children[:currentNumberOfChildren])
	done2 <- true
}

func node(index int) {
	var maxNumberOfChildren = len(neighbours[index])
	var currentNumberOfChildren = 0
	var children = make([]int, maxNumberOfChildren)
	for i := 0; i < maxNumberOfChildren; i++ {
		children[i] = -1
	}

	var neverReceivedSearch = true
	var father = -1

	for {
		msg, more := <-channels[index]
		if more {
			if msg.msgType == search && neverReceivedSearch {
				// received 'search' for the first time
				neverReceivedSearch = false
				// father = sender
				father = msg.sender
				// send 'search' to all neighbours
				for i := 0; i < len(neighbours[index]); i++ {
					var neighbour = neighbours[index][i]
					channels[neighbour] <- Message{search, index}
				}
				// send `your are my father` to parent
				channels[father] <- Message{youAreMyFather, index}
				done <- true
			} else if msg.msgType == youAreMyFather {
				// append children
				children[currentNumberOfChildren] = msg.sender
				currentNumberOfChildren++
			}
		} else {
			break
		}
	}
	fmt.Println(index, ": parent = ", father, ", children = ", children[:currentNumberOfChildren])
	done2 <- true
}

func main() {
	var numberOfNodes int
	fmt.Scanf("%d", &numberOfNodes)
	channels = make([]chan Message, numberOfNodes)
	done = make(chan bool, numberOfNodes)
	done2 = make(chan bool, numberOfNodes)
	neighbours = make([][]int, numberOfNodes)
	for i := 0; i < numberOfNodes; i++ {
		channels[i] = make(chan Message, numberOfNodes*numberOfNodes)
		var numberOfNeighbours int
		fmt.Scanf("%d", &numberOfNeighbours)
		neighbours[i] = make([]int, numberOfNeighbours)
		for j := 0; j < numberOfNeighbours; j++ {
			var neighbour int
			fmt.Scanf("%d", &neighbour)
			neighbours[i][j] = neighbour
		}
	}

	// start goroutines
	go root()
	for i := 1; i < numberOfNodes; i++ {
		go node(i)
	}

	// wait until last node finds his parent
	for i := 0; i < numberOfNodes; i++ {
		_ = <-done
	}

	// stop simulation
	for i := 0; i < numberOfNodes; i++ {
		close(channels[i])
	}

	// wait until all nodes print results
	for i := 0; i < numberOfNodes; i++ {
		_ = <-done2
	}

}
